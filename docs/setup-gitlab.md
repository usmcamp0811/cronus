# How to Setup Gitlab

### Create a new repository on Gitlab.

1. Click New Porject on the Gitlab Projects Page

    ![](./img/gitlab-new-project.png)

2. Create Blank Project

    ![](./img/gitlab-blank-project.png)

3. Complete Blank Project Form and Click the Green Button when Done.

    ![](./img/gitlab-create-project.png)

4. Follow the **Push an existing folder** instructions in the new project. 

```bash
cd <your project folder created by cookiecutter>
git init
git remote add origin <the url to your new git repo>
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Enable CI/CD Pipelines.

This should just work out of the box once you **push** your project to Gitlab. However if you do not have a **CI/CD** tab pictured below follow these instructions.

![](./img/gitlab-project-cicd.png)

1. Click Settings inside your Project Page.

![](./img/gitlab-settings.png)

2. Click Visiblity Settings

![](./img/gitlab-settings-visibility.png)

3. Make sure the **Pipelines** button is turned on.

![](./img/gitlab-settings-pipelines.png)
