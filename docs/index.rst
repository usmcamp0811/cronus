.. Automatic Testing documentation master file, created by
   sphinx-quickstart on Mon Aug  3 12:02:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to a Cookiecutter Project Template for: Python w/ Sphinx, Pytest, Gitlab CI/CD and Docker
=============================================

This project is meant to be a starting point for new Python projects. It comes with Gitlab CI/CD Pipelines 
already enabled. The Pipelines are configured to automatically run Pytest and generate documentation 
using Sphinx. The Sphinx documentation is hosted on Gitlab Pages. Finally there are Docker files to 
make it easy to containerize the project for both development and deployment. 

.. toctree::
   :maxdepth: 2
   :glob:

   setup-gitlab.md
   pytest-tutorial.md
   docker-development.md
   google_docs_example.rst
   numpy_docs_example.rst

    


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
