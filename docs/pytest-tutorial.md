# Test My Code!

If you are using this project template, you get Pytest preconfigured to be run with every commit / push to Gitlab. 
For this to work though, you still need to manually write test cases for your code. An example test file is included 
at `./<project>/tests/test_hello_world.py`. The tests can be run locally from the root of the project with the following
`poetry run pytest`. 

## What do Test look like?

```python
import pytest  #<<< This is required for Pytest to know to run this file

# You need to import your code that you want to test
from <my_project>.<my_module> import <my_functions_to_test> 

# write at least one function per function you want to test.
def test_<my_function_name>():
    my_test_output = <my_function_nae>(input1="Known Value")
    # use an assert to cause an error if your function didn't do what it was supposed to do.
    assert "Expected Output" == my_test_output
    # NOTE: you can have multiple assertions per function. This could be used to test the same function in different ways.
    
```

## Where Do I Put My Test Code?

You don't have to put your code here but it is *strongly* recommended.

```bash
.../<project_name>
 ├──docker-compose.yml
 ├──Dockerfile
 ├──docs
 │  ├──conf.py
 │  ├──<project_name>.rst
 │  ├──hello_world.rst
 │  └──index.rst
 ├──entrypoint.sh
 ├──<project_name>
 │  ├──__init__.py
 │  ├──<project_name>.py
 │  └──hello_world.py
 ├──poetry.lock
 ├──pyproject.toml
 ├──README.md
 └──tests    <<<<<----------- Put your test code here
    ├──test_<project_name>.py
    └──test_hello_world.py
```

## How Do I Run My Tests?

If you want to manually run your tests on your local machine, you can run the following command from the root 
of your project.

```bash
# at .../<project_name>
# run pytest with the verbose tag
poetry run pytest -v

# for all the arguments you can pass to pytest run the following.
poetry run pytest -h 
```


