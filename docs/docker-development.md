# Python Development using Docker

## Why?

If you are making something for just yourself, you might not need to develop in a container. However,
if you expect to distribute your code to others, it will save you the trouble of telling them "it 
worked on my machine". If you are new to software development and are unfamiliar with this problem,
just understand it is a huge problem because every computer environment is undoubtedly going to be
different in some way, and you can't always plan for every possibility. 

## How?

So I have tried to make it as simple as possible with this cookiecutter project by including both
a `Dockerfile` and a `docker-compose.yml`. The `Dockerfile` is used to define your development/deployment environment. You can kind of think of it as a shell script that will start with a bare 
bare bones system configuration and install only the things your program needs. The 
`docker-compose.yml` is a file used to easily build and launch your docker image in a way 
that will make it easy to develop against. For more information on Docker checkout 
[this](https://www.youtube.com/playlist?list=PLCNla0W4k0xs5gxxXxmNDwYZKS8FycnV2) YouTube 
playlist I have created.


## Dockerfile Explained

```Dockerfile
FROM python:3.8
```

I am using the latest Python version here because I think its generally to start new projects with
the latest and greatest version of things to try and delay obsolescence issues as much as possible. 
You could use any version of Python that you want or a totally different base image. To find 
other publicly available Docker images checkout, [Docker Hub](https://hub.docker.com/).


```Dockerfile
COPY pyproject.toml /build/

WORKDIR /build/
```

I copy over the `pyproject.toml` used by `poetry` to configure the Python environment first because
if you do it later and you make any changes to your code you will have to rebuild the entire image
and this can be very time consuming. I put the file in a directory called `build` but it could 
go in any directory. After copying the file over I set the current working directory (the place all commands will be run from) to be that directory.

```Dockerfile
USER cronus
```

This command is not really necessary, but I have the image launch Jupyter Lab and that doesn't 
like to be run as `root`. You could use any name here if you wanted. I just chose `cronus` because
I have zero imagination.

```Dockerfile
RUN export PATH=$PATH:/home/cronus/.local/bin \
&&  pip install --user poetry pip jupyterlab --user --upgrade \
&&  python -m poetry config virtualenvs.in-project true \
&&  python -m poetry install \
&&  poetry run pip install ipykernel \
&&  poetry run python -m ipykernel install --user --name={{cookiecutter.project_name}} 
```

What the hell is this?! Whats the deal with all the `&&`??!! Ok so one thing to know about Docker
is that the images are created in layers, with each layer being effectively a line in the `Dockerfile` 
Details about this can be found elsewhere but the key take away here is that you should limit the 
number of `RUN` commands in your `Dockerfile`. The easiest way to do this is to pass multi-line
commands as `&&`. The `\` is used to join lines together and make things easier to read. So what's
going on here? I add the `cronus` user's `.local/bin` directory to the current path because this
is where executables will be placed. This matters because `poetry` is going to get put there and 
if we don't then we have to use the full path and I just didn't want to do that. 

So at a system level (inside the docekr image) I install `jupyterlab`, `pip`, and `poetry`. This will
install those packages and upgrade them to the most recent version. Poetry is needed because we will
be using it as our package manager. I chose to use Poetry because the Python package ecosystem is 
one hot mess and Poetry is making things easier to maange. It's still fairly new and I don't love 
every thing about it but its very helpful. I suggest you checkout their [documentation](https://python-poetry.org/docs/). 

`poetry install` is used to install all of your programms dependencies (Python) into the container. As long as you follow the Poetry documentation for adding dependencies, you should be good here. Next
I do `pip install ipykernel` and this is done solely so that the virtual environment created by
Poetry will be accessible to the Jupyter Lab inside the image. 

```Dockerfile
COPY . /build
ENTRYPOINT ["/bin/bash", "/build/entrypoint.sh" ]
```
Finally I copy over the rest of the files from the project. To emphasize what I stated earlier it is
important to copy over the rest of the files from the project last because this will keep you
from having to rebuild the image every time you make a change to your code. The entrypoint command
is a script that will get run everytime that a container using the image is run. 



### Entrypoint

```bash
#!/bin/sh

[[ "$1" == "jlab" ]] && /home/cronus/.local/bin/jupyter lab --ip 0.0.0.0 --port 8888 && exit 0


# will return a repl of your environment.. or you can pass arguments and have it run a file
/build/.venv/bin/python $@
```

This is a super simple shell script I wrote to launch Jupyer Lab if the command `jlab` is passed. 
If the command is not given then it will (using your Python virtual environment) run the file 
or command given. You might be able to think of this as being an executable. *NOTE: Any file name passed would need to be inside the container*


## Docker for Development

```yml
        volumes:
            - .:/build
```
Alright so how the hell do I write code and have it work in my container?! Ok so in the `docker-compose.yml`
file we specify a host volume that will map the contents of the current project directory to `/build` 
inside our running container. But what does this mean?! It means that all changes to files that occur
inside of the project directory will immediately happen to files inside the running container.
SO... you can use your favorite text editor (nvim is mine) to edit your Python code and using

```bash
docker exec -it dev_env my_python_file.py
```

you can see how your code acts. Additionally since I have Jupyter Lab running you can also use this
to test your code interactively. Finally you can have the `test_env` container to test your code as 
it would be on your users system.


## Summary

Ok so I have tried to keep things as simple as I could while still providing some insights. To
get this to work flaulessly it might take some work but I've gotten you about 90% of the way. 
If you have problems or questions feel free to reach out to me. I've been doing this long enough
to have experienced a good of bugs. 

Good luck! 
