# Welcome to a Cookiecutter Project Template for: Python w/ Sphinx, Pytest, Gitlab CI/CD and Docker

This project is meant to be a starting point for new Python projects. It comes with Gitlab CI/CD Pipelines 
already enabled. The Pipelines are configured to automatically run Pytest and generate documentation 
using Sphinx. The Sphinx documentation is hosted on Gitlab Pages. Finally there are Docker files to 
make it easy to containerize the project for both development and deployment. 

## Install Dependencies

```bash
$> pip install poetry cookiecutter --user --upgrade
```

## How to Use

In a directory that you want to store your new Python project run the following command and answer the questions with
respect to your project.

```bash
# Standard Python project
$> python -m cookiecutter https://gitlab.com/usmcamp0811/cronus.git
```

```bash
# Flask/GraphQL project
$> python -m cookiecutter https://gitlab.com/usmcamp0811/cronus.git -c graphql-api
```


# [Additional Documentation](https://usmcamp0811.gitlab.io/cronus/).

