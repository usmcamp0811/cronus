#!/bin/sh

stat /build/.venv/ || (python -m poetry config virtualenvs.in-project true && python -m poetry install)

[[ "$1" == "jlab" ]] && /home/cronus/.local/bin/jupyter lab --ip 0.0.0.0 --port 8888 && exit 0


# will return a repl of your environment.. or you can pass arguments and have it run a file
/build/.venv/bin/python $@
