{{cookiecutter.project_name}}
==================================

This is my awesome new Python App. Add documentation here...and *DON'T* forget to add docstrings to your code!!

.. automodule:: {{cookiecutter.project_name}}.{{cookiecutter.project_name}}
    :members:
