.. Automatic Testing documentation master file, created by
   sphinx-quickstart on Mon Aug  3 12:02:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{cookiecutter.project_name}}'s documentation!
=============================================

Add Documentation here...

.. toctree::
   :maxdepth: 2
   :glob:

   {{cookiecutter.project_name}}.rst
   hello_wolrd.rst

    


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
