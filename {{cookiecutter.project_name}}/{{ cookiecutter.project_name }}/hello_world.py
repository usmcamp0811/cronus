import datetime
import os
import logging
import maya


def hello(user: str = "World"):
    """
    A simple hello world function to give us some code to test and document.
    
    Args:
        user (str): The thing we say hello to...
    """
    print(f"Hello {user}!")


def add(a: float, b: float) -> float:
    """
    Another super awesome function to prove that 1 + 1 = 4

    Args:
        a (float): The first number
        b (float): The second number

    Returns:
        The sum of ``a + b``
    """
    logging.warning('Watch out!')  # will print a message to the console
    logging.info('I told you so')  # will not print anything
    c = a + b
    return c


def mult(a: float, b: float) -> float:
    """    
    You guessed it another simple function to multiply two numbers together.

    Args:
        a (float): The first number
        b (float): The second number

    Returns:

        The product of ``a * b``
    """
    c = a * b
    return c


def div(a: float, b: float) -> float:
    c = a / b
    return c


def sub(a: float, b: float) -> float:
    c = a - b
    return c


def age(dob: str) -> int:
    """
    A function to calculate your age given a date of birth.

    Args:

        dob (str): Your date of birth in any human readable format.

    Returns:
    
        Your age in years.
    """
    dob = maya.when(dob)
    age = maya.MayaInterval(start=dob, end=maya.now())
    print("You were born ", maya.humanize.naturaltime(age.duration))
    return int(age.duration.split(" ")[0])


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Simple program to test Gitlab CI/CD")
    parser.add_argument(
        "-u",
        "--user-name",
        action="append",
        help="This is the name that will be printed in the greeting",
    )
    parser.add_argument(
        "-a", "--A", type=float, help="A value to have something done to."
    )
    parser.add_argument("-b", "--B", type=float, help="B value to do something to A.")
    parser.add_argument(
        "-f",
        "--function",
        dest="function",
        choices=["add", "mult", "div", "sub"],
        help="The name of the function to run",
    )
    parser.add_argument(
        "-dob", dest="dob", type=str, help="Human formated date of birth"
    )
    args = parser.parse_args()

    if args.user_name:
        hello(user=args.user_name[0])
    else:
        hello()

    if args.function:
        funcs = dict(add=add, mult=mult, div=div, sub=sub)
        print("Your answer is: ", funcs[args.function](a=args.A, b=args.B))

    if args.dob:
        age(args.dob)
